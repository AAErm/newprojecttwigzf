<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\Router\Http\Regex;
use Zend\ServiceManager\Factory\InvokableFactory;
use Application\Route\StaticRoute;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,

                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller'    => Controller\IndexController::class,
                        'action'        => 'index',
                    ],
                ],
            ],
//            'about' => [
//                'type' => Literal::class,
//                'options' => [
//                    'route'    => '/about',
//                    'defaults' => [
//                        'controller' => Controller\IndexController::class,
//                        'action'     => 'about',
//                    ],
//                ],
//            ],
            'ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/ajax',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'ajax',
                    ],
                ],
            ],
//            'download' => [
//                'type'    => Segment::class,
//                'options' => [
//                    'route'    => '/download[/:action]',
//                    'defaults' => [
//                        'controller'    => Controller\DownloadController::class,
//                        'action'        => 'index',
//                    ],
//                ],
//            ],
//            'doc' => [
//                'type' => Regex::class,
//                'options' => [
//                    'regex'    => '/doc(?<page>\/[a-zA-Z0-9_\-]+)\.html',
//                    'defaults' => [
//                        'controller' => Controller\IndexController::class,
//                        'action'     => 'doc',
//                    ],
//                    'spec'=>'/doc/%page%.html'
//                ],
//            ],
//            'static' => [
//                'type' => StaticRoute::class,
//                'options' => [
//                    'dir_name'         => __DIR__ . '/../view',
//                    'template_prefix'  => 'application/index/static',
//                    'filename_pattern' => '/[a-z0-9_\-]+/',
//                    'defaults' => [
//                        'controller' => Controller\IndexController::class,
//                        'action'     => 'static',
//                    ],
//                ],
//            ],
//            'barcode' => [
//                'type' => Segment::class,
//                'options' => [
//                    'route' => '/barcode[/:type/:label]',
//                    'constraints' => [
//                        'type' => '[a-zA-Z][a-zA-Z0-9_-]*',
//                        'label' => '[a-zA-Z0-9_-]*'
//                    ],
//                    'defaults' => [
//                        'controller' => Controller\IndexController::class,
//                        'action' => 'barcode',
//                    ],
//                ],
//            ]
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
//            Controller\DownloadController::class => InvokableFactory::class,
        ],
    ],
    // The following registers our custom view 
    // helper classes in view plugin manager.
    'view_helpers' => [
        'factories' => [
            View\Helper\NotFoundReason::class => InvokableFactory::class,
            View\Helper\GetClass::class => InvokableFactory::class
        ],
        'aliases' => [
            'notFounfdReason' => View\Helper\NotFoundReason::class,
            'getClass' => View\Helper\NotFoundReason::class,
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout' => __DIR__ . '/../view/layout/layout.twig',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
