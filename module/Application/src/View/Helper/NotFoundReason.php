<?php

namespace Application\View\Helper;

use Zend\Mvc\Application;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Helper\HelperInterface;
use Zend\View\Renderer\RendererInterface as Renderer;

/**
 * @package Application\View\Helper
 */
class NotFoundReason extends AbstractHelper
{
    public function __invoke($reason) {
        switch ($reason) {
            case Application::ERROR_CONTROLLER_CANNOT_DISPATCH:
                $reasonMessage = 'ERROR_CONTROLLER_CANNOT_DISPATCH';
                break;
            case Application::ERROR_MIDDLEWARE_CANNOT_DISPATCH:
                $reasonMessage = 'ERROR_MIDDLEWARE_CANNOT_DISPATCH';
                break;
            case Application::ERROR_CONTROLLER_NOT_FOUND:
                $reasonMessage = 'ERROR_CONTROLLER_NOT_FOUND';
                break;
            case Application::ERROR_CONTROLLER_INVALID:
                $reasonMessage = 'ERROR_CONTROLLER_INVALID';
                break;
            case Application::ERROR_ROUTER_NO_MATCH:
                $reasonMessage = 'ERROR_ROUTER_NO_MATCH';
                break;
            default:
                $reasonMessage = '404';
                break;
        }

        return $reasonMessage;
    }
}
