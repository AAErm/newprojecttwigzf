<?php
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;

/**
 * @package Application\View\Helper
 */
class GetClass extends AbstractHelper
{
    public function __invoke($class)
    {
        return get_class($class);
    }
}
