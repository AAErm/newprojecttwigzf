<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel(['azaza' => '12345678']);
    }

    public function ajaxAction()
    {
        return new ViewModel(['data' => '6543211k' ]);
    }
}
